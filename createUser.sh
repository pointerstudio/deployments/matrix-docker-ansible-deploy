#!/bin/bash

# ------------------------------------------------------------ HELLO
cat <<EOF
STUDIO       _       _
 _ __   ___ (_)_ __ | |_ ___ _ ____/\__
| '_ \ / _ \| | '_ \| __/ _ \ '__\\    /
| |_) | (_) | | | | | ||  __/ |  /_  _\\
| .__/ \___/|_|_| |_|\__\___|_|    \/
|_|
      presents: $0

EOF

# ------------------------------------------------------------ HELPER FUNCTIONS
decision() {
    # get default
    if [ "$2" = 0 ]; then
        default="yes"
    else
        default="no"
    fi

    # should we go autopilot
    if [ "${REMEMBORY_AUTOPILOT}" = 1 ];
    then
        echo "$1?"
        return $2
    fi

    echo ""
    echo "----------------------------------"
    echo "| $0 NEEDS A DECISION"
    echo "| "

    echo "| $1?"
    read -r -p "| Do you want? (y)es / (n)o / (d)efault - $default " input

    case $input in
        [yY][eE][sS]|[yY])
            return 0
            ;;
        [nN][oO]|[nN])
            return 1
            ;;
        [dD])
            return $2
            ;;
        *)
            return 1
            ;;
    esac
}

# ------------------------------------------------------------ VARIABLES
# dynamic
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# defaults
REMEMBORY_AUTOPILOT=0
PROBABLY_YES=0
PROBABLY_NOT=1

read -p 'enter new user: ' MATRIX_USERNAME
unset MATRIX_PASSWORD
prompt="enter password: "
while IFS= read -p "$prompt" -r -s -n 1 char
do
    if [[ $char == $'\0' ]]
    then
        break
    fi
    prompt='*'
    MATRIX_PASSWORD+="$char"
done

IS_ADMIN='no'

if decision "should this be an admin" $PROBABLY_NOT;
then
    IS_ADMIN='yes'
fi

ansible-playbook -i inventory/hosts setup.yml --extra-vars="username=$MATRIX_USERNAME password=$MATRIX_PASSWORD admin=$IS_ADMIN" --tags=register-user
